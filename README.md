# How to start

## Prerequisites
- **Docker** (for the PHP and MySQL backend)
- **PHP** (>7.0)
- **Composer**
- **NodeJs**
- **Yarn** or **NPM** (preferred)

## Installation
1. Rename the `.env.example` to `.env`
2. Run these commands:
``` shell
composer install --ignore-platform-reqs
npm install
npm run dev

./vendor/bin/sail up -d
./vendor/bin/sail artisan migrate --seed
```
In case that some error occurred at these commands, rerun them with `sudo` in front.

## Login
The default users are:

| Name        |  E-Mail       | Password |
|:-----------:|--------------|-------|
| Admin User | admin@test.de | admin |
| Normal User | test@test.de | test |

<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TicketController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::get('tickets', [TicketController::class, 'userTickets'])->name('userTickets');
    //Route::resource('ticket', TicketController::class)->except(['index', 'edit']);
    Route::get('ticket/create', [TicketController::class, 'create'])->name('ticket.create');
    Route::post('ticket', [TicketController::class, 'store'])->name('ticket.store');
    Route::get('ticket/{ticket_id}', [TicketController::class, 'show'])->name('ticket.show');
    Route::put('ticket/{ticket_id}', [TicketController::class, 'update']);
    Route::patch('ticket/{ticket_id}', [TicketController::class, 'update'])->name('ticket.update');
    Route::delete('ticket/{ticket_id}', [TicketController::class, 'destroy'])->name('ticket.destroy');
    Route::get('ticket/{ticket_id}/close', [TicketController::class, 'close'])->name('closeTicket');
    Route::post('ticket/{ticket_id}/comment', [TicketController::class, 'postComment']);

    Route::group(['prefix' => 'admins', 'middleware' => 'admin'], function () {
        Route::get('/', [AdminController::class, 'show'])->name('allAdmins');
        Route::get('download', [AdminController::class, 'download'])->name('download');
        Route::post('upload', [AdminController::class, 'upload'])->name('upload');
    });

    Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
        Route::get('tickets', [TicketController::class, 'index'])->name('adminTickets');
        Route::get('{user_id}/status/{isAdmin}', [AdminController::class, 'changeAdminstatus']);
    });
});

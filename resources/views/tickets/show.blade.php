<div class="card card-default">
    <span class="close sidebar-close">
        <i class="fa fa-times"></i>
    </span>
    <div class="card-heading">
        <h1>
            {{ $title }}
        </h1>
    </div>

    <div class="card-body">
        <div class="tw-flex detail-view">
            <div class="col-md-3 control-label">Category</div>

            <div class="col-md-9">
                <div class="content">{{ $category }}</div>
            </div>
        </div>

        <div class="tw-flex detail-view">
            <div class="col-md-3 control-label">Priority</div>

            <div class="col-md-9">
                <div class="content">{{ $priority }}</div>
            </div>
        </div>

        <div class="tw-flex detail-view">
            <div class="col-md-3 control-label">Refer to</div>

            <div class="col-md-9">
                <div class="content">{{ $referrer->name }}</div>
            </div>
        </div>

        <div class="tw-flex detail-view">
            <div class="col-md-3 control-label">Message</div>

            <div class="col-md-9">
                <div class="content">{{ $message }}</div>
            </div>
        </div>

        <div class="tw-flex comments tw-flex-col">
            <h2>Comments</h2>
            @if (count($comments) > 0)
                @foreach($comments as $comment)
                    <div class="tw-flex tw-justify-between content comment header">
                        <span>{{ $comment->user->name }}</span>
                        <span>{{ $comment->created_at }}</span>
                    </div>
                    <div class="content comment tw-mb-3">{{ $comment->comment }}</div>
                @endforeach
            @else
                <p class="tw-pt-2">There are no comments at the moment</p>
            @endif
        </div>

        <form class="form-horizontal" id="newCommentForm" role="form" method="POST"
              action="{{ action([App\Http\Controllers\TicketController::class, 'postComment'],['ticket_id' => $id]) }}" hidden>
            @csrf
            <div class="col-12">
                <textarea rows="10" id="newComment" class="form-control" name="newComment" required></textarea>
            </div>
        </form>

        <div class="tw-flex detail-view">
            <button class="btn btn-primary tw-mr-2" id="btnAddComment">
                <i class="fa fa-btn fa-ticket-alt"></i> Add Comment
            </button>
            @if($status !== 'closed')
                <form action="{{ route('closeTicket', $id) }}" method="get">
                    @csrf
                    <button class="btn btn-danger tw-mr-2" id="btnTicketClose">
                        <i class="fa fa-times"></i> Close Ticket
                    </button>
                </form>
            @endif
            @if($deletable)
                <form action="{{ route('ticket.destroy', ['ticket_id' => $id]) }}" method="post">
                    @csrf
                    @method('delete')
                    <button class="btn btn-danger tw-mr-2" id="btnTicketDelete">
                        <i class="fab fa-gripfire"></i> Delete Ticket
                    </button>
                </form>
            @endif
        </div>
    </div>
</div>

@extends('layouts.app')

@section('title', 'All Tickets')

@section('content')
    <div class="container-fluid">
        <div class="tw-flex tw-justify-between heading">
            <h1>
                <i class="fa fa-ticket-alt"></i>
                Tickets
            </h1>
            <button class="btn btn-success tw-m-2" id="btnNewTicket">
                <i class="fa fa-ticket-alt"></i>
                Add ticket
            </button>
        </div>
        @if ($tickets->isEmpty())
            <p>There are currently no tickets.</p>
        @else
            <div class="table-responsive table-responsive-sm">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="t-4">Title</th>
                        <th class="t-1">Category</th>
                        <th class="t-1">Status</th>
                        <th class="t-1">Priority</th>
                        <th class="t-1_5">Last Updated</th>
                        <th class="t-1">Referrer</th>
                        <th class="t-1" style="text-align:center">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($tickets as $ticket)
                        <tr>
                            <td>
                                <span class="text-{{ $ticket->priority }}">
                                    {{ $ticket->title }}
                                </span>
                            </td>
                            <td>
                                {{ $ticket->category->name }}
                            </td>
                            <td>
                                @if ($ticket->status === 'open')
                                    <span class="badge badge-success">{{ $ticket->status }}</span>
                                @else
                                    <span class="badge badge-danger">{{ $ticket->status }}</span>
                                @endif
                            </td>
                            <td class="tw-font-bold ">
                                @if ($ticket->priority === 'low')
                                    <span class="text-secondary">{{ $ticket->priority }}</span>
                                @elseif ($ticket->priority === 'medium')
                                    <span class="text-primary">{{ $ticket->priority }}</span>
                                @else
                                    <span class="text-danger">{{ $ticket->priority }}</span>
                                @endif
                            </td>
                            <td>{{ $ticket->updated_at }}</td>
                            <td>
                                <form method="POST" action="{{ action([App\Http\Controllers\TicketController::class, 'update'],['ticket_id' => $ticket->id]) }}">
                                    @csrf
                                    @method('PATCH')
                                    <label class="tw-mb-0">
                                        <select class="custom-select form-control w-auto" name="referrer" id="referrer_{{ $ticket->id }}"
                                                onchange="form.submit()">
                                            @foreach($users as $user)
                                                @if($user == $ticket->referrer)
                                                    <option value="{{ $user->id }}" selected>{{ $ticket->referrer->name }}</option>
                                                @else
                                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </label>
                                </form>
                            </td>
                            <td>
                                <div class="tw-flex tw-justify-around">
                                    <div class="btn btn-primary details" id="ticket_{{ $ticket->id }}">Details</div>
                                    <a class="btn btn-danger" href="{{ action([App\Http\Controllers\TicketController::class, 'close'],['ticket_id' => $ticket->id]) }}">Close</a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
@endsection

@section('new')
    @include('tickets.create')
@endsection

<div class="card card-default">
    <span class="close sidebar-close">
        <i class="fa fa-times"></i>
    </span>
    <div class="card-heading">
        <h1>
            Open New Ticket
        </h1>
    </div>

    <div class="card-body">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('ticket.store') }}">
            @csrf

            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                <label for="title" class="col-md-4 control-label">Title</label>

                <div class="col-md-8">
                    <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" required/>

                    @if ($errors->has('title'))
                        <span class="help-block"><strong>{{ $errors->first('title') }}</strong></span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                <label for="category" class="col-md-4 control-label">Category</label>

                <div class="col-md-8">
                    <select id="category" type="category" class="form-control custom-select" name="category" required>
                        <option value="">Select Category</option>
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('category'))
                        <span class="help-block"><strong>{{ $errors->first('category') }}</strong></span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('priority') ? ' has-error' : '' }}">
                <label for="priority" class="col-md-4 control-label">Priority</label>

                <div class="col-md-8">
                    <select id="priority" type="" class="form-control custom-select" name="priority" required>
                        <option value="">Select Priority</option>
                        <option value="low">Low</option>
                        <option value="medium">Medium</option>
                        <option value="high">High</option>
                    </select>

                    @if ($errors->has('priority'))
                        <span class="help-block"><strong>{{ $errors->first('priority') }}</strong></span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('referredUserId') ? ' has-error' : '' }}">
                <label for="referredUserId" class="col-md-4 control-label">Refer to</label>

                <div class="col-md-8">
                    <select id="referredUserId" type="" class="form-control custom-select" name="referredUserId" required>
                        <option value="">Select User</option>
                        @foreach ($users as $user)
                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('referredUserId'))
                        <span class="help-block"><strong>{{ $errors->first('referredUserId') }}</strong></span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                <label for="message" class="col-md-4 control-label">Message</label>

                <div class="col-md-8">
                    <textarea rows="10" id="message" class="form-control" name="message" required>{{ old('message') }}</textarea>

                    @if ($errors->has('message'))
                        <span class="help-block"><strong>{{ $errors->first('message') }}</strong></span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-btn fa-ticket-alt"></i> Open Ticket
                    </button>
                    <button class="btn btn-danger sidebar-close">
                        <i class="fa fa-btn fa-times"></i> Abort
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

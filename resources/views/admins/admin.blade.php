@extends('layouts.app')

@section('title', 'All Admins')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card card-default">
                <div class="card-heading tw-pl-5">
                    Edit Admins
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Admin</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>
                                    @if ($user->isAdmin)
                                        <i class="fa fa-check"></i>
                                    @else
                                        <i class="fa fa-times"></i>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ url('admin/' . $user->id . '/status/' . $user->isAdmin) }}" class="btn btn-primary">Change</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row tw-flex tw-justify-evenly">
        <form method="get" action="{{ route('download') }}">
            @csrf
            <button class="btn btn-primary" type="submit">Download all admins</button>
        </form>
        <form method="post" action="{{ route('upload') }}" class="tw-flex" enctype="multipart/form-data">
            @csrf
            <div class="custom-file">
                <input type="file" id="file" name="file" class="custom-file-input" oninput="document.querySelector('label.custom-file-label').textContent = this.files[0].name;"/>
                <label class="custom-file-label" for="file"></label>
            </div>
            <button type="submit" class="btn btn-secondary tw-w-2/5 upload"><i class="fa fa-upload tw-pr-1.5"></i>Upload</button>
        </form>
    </div>
@endsection

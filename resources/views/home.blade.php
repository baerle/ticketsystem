@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="card card-default">
                <div class="card-heading">Dashboard</div>

                <div class="card-body">
                    @if (Auth::user()->isAdmin)
                        <p>
                            See all <a href="{{ route('adminTickets') }}">tickets</a>
                        </p>
                        <p>
                            See your own <a href="{{ route('userTickets') }}">tickets</a>
                        </p>
                        <p>
                            <a href="{{ route('allAdmins') }}" >Configure Admins</a>
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

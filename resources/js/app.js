'use strict'
require('./bootstrap');

let sidebar;

document.addEventListener("DOMContentLoaded", () => {
    sidebar = document.getElementById('sidebar');
    for (const ticket of document.getElementsByClassName('details')) {
        ticket.addEventListener('click',
            () => openDetailsSidebar(ticket.id.split('_')[1]));
    }

    document.getElementById('btnNewTicket')?.addEventListener(
        'click', () => openCreateSidebar());
});

async function openDetailsSidebar(ticketId) {
    sidebar.innerHTML = await fetchAPI('/ticket/' + ticketId);
    openSidebar();

    document.getElementById('btnAddComment').addEventListener(
        'click', () => addComment());
}

async function openCreateSidebar() {
    sidebar.innerHTML = await fetchAPI('/ticket/create');
    openSidebar();
}

function openSidebar() {
    sidebar.hidden = false;
    addSidebarListeners();
}

function addSidebarListeners() {
    for (const e of document.getElementsByClassName('sidebar-close')) {
         e.addEventListener(
        'click', e => {
            e.preventDefault();
            sidebar.hidden = 'hidden';
        });
    }
}

function addComment() {
    document.getElementById('newCommentForm').hidden = false;

    const textArea = document.getElementById('newComment');
    if (textArea.value !== '') {
        textArea.form.submit();
    } else {
        textArea.focus();
    }
}

async function fetchAPI(url) {
    return await fetch(
        url, {
            method: 'GET',
            headers: {
                'Accept': 'text/html',
            },
            redirect: 'follow',
        },
    ).then(response => {
        if (response.status >= 200 && response.status < 300) {
            return response.text();
        }
    });
}

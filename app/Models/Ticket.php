<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;

    protected $table = 'ticket';

    protected $fillable = [
        'title',
        'message',
        'priority',
        'categoryId',
        'status',
        'creatorUserId',
        'referredUserId',
    ];

    public function category() {
        return $this->belongsTo(Category::class, 'categoryId');
    }

    public function referrer() {
        return $this->belongsTo(User::class, 'referredUserId');
    }

    public function creator() {
        return $this->belongsTo(User::class, 'creatorUserId');
    }

    public function comments() {
        return $this->hasMany(Comment::class, 'ticketId');
    }
}

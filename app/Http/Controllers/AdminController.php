<?php

declare(strict_types=1);


namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    public function show() {
        $users = User::all();

        return view('admins.admin', ['users' => $users]);
    }

    public function download() {
        $admins = User::all();
        $admins->makeHidden(['email_verified_at', 'created_at']);   // 'updated_at' is missing on purpose to be able to update it manually
        $ok = Storage::put(date('Admins-Y-m-d-H:i:s').'.txt', $admins);
        if ($ok) {
            return Storage::download(date('Admins-Y-m-d-H:i:s') . '.txt');
        } else {
            return response(status: 500);
        }
    }

    public function upload(Request $request) {
        try {
            $file = $request->file();
            $json = File::get($file['file']->getRealPath());
            $users = json_decode($json, true);
            foreach ($users as $user) {
                User::updateOrCreate(['id'=> $user['id']], $user);
            }
        } catch (FileNotFoundException $e) {
        }

        return response()->redirectToRoute('allAdmins');
    }

    public function changeAdminstatus(int $id, bool $status) {
        $user = User::findOrFail($id);
        $user->isAdmin = !$status;
        $user->save();

        return response()->redirectToRoute('allAdmins');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard if user is Admin.
     */
    public function index()
    {
        if (Auth::user()->isAdmin === 1) {
            return view('home');
        }
        return response()->redirectToRoute('userTickets');
    }
}

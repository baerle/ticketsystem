<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Comment;
use App\Models\Ticket;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class TicketController extends Controller
{
    private const REFERRER = 'referrer';

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(): Factory|View|Application
    {
        session(['overview-url' => \request()->url()]);
        return view(
            'tickets.indexAdmin',
            [
                'tickets' => Ticket::orderByRaw('FIELD(status, "open", "closed", "deleted")')
                    ->orderByRaw('FIELD(priority, "high", "medium", "low")')
                    ->paginate(15),
                'categories' => Category::all(),
                'user' => Auth::user(),
                'users' => User::all(),
            ]
        );
    }

    /**
     * Show the form for creating a new ticket
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view(
            'tickets.create',
            [
                'categories' => Category::all(),
                'user' => Auth::user(),
                'users' => User::where('id', '!=', Auth::id())->get(),
            ]
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function userTickets(): Factory|View|Application
    {
        session(['overview-url' => \request()->url()]);
        return view(
            'tickets.index',
            [
                'tickets' => Ticket::where('creatorUserId', Auth::id())
                    ->orWhere('referredUserId', Auth::id())
                    ->orderByRaw('FIELD(status, "open", "closed", "deleted")')
                    ->orderByRaw('FIELD(priority, "high", "medium", "low")')
                    ->paginate(15),
                'categories' => Category::all(),
                'user' => Auth::user(),
                'users' => User::all(),
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
           'title' => 'required|string|max:255',
           'message' => 'required|string',
           'priority' => 'required|string',
            'category' => 'required|integer',
            'referredUserId' => 'required|integer',
        ]);

        if ($request->input('referredUserId') != Auth::id()) {
            Ticket::create(
                [
                    'title' => $request->input('title'),
                    'message' => $request->input('message'),
                    'priority' => $request->input('priority'),
                    'categoryId' => $request->input('category'),
                    'status' => 'open',
                    'creatorUserId' => Auth::id(),
                    'referredUserId' => $request->input('referredUserId'),
                    'categories' => Category::all(),
                    'users' => User::all(),
                ]
            );
        }
        return redirect(session('overview-url'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function show(int $id)
    {
        $ticket = Ticket::findOrFail($id);
        return view(
            'tickets.show',
            [
                'id' => $id,
                'title' => $ticket->title,
                'category' => $ticket->category->name,
                'priority' => $ticket->priority,
                'referrer' => $ticket->referrer,
                'creator' => $ticket->creator,
                'message' => $ticket->message,
                'comments' => $ticket->comments,
                'status' => $ticket->status,
                'deletable' => (Auth::id() === $ticket->creatorUserId || Auth::id() === $ticket->referredUserId) && $ticket->status !== 'deleted',
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, int $id)
    {
        $ticket = Ticket::findOrFail($id);

        $ticket->referredUserId = $request->input(self::REFERRER) ?? $ticket->referredUserId;

        $ticket->save();

        return redirect(session('overview-url'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id)
    {
        $ticket = Ticket::findOrFail($id);
        $ticket->status = 'deleted';

        $ticket->save();

        // $ticket->delete();   // alternatively

        return redirect(session('overview-url'));
    }

    /**
     * Set the status of the ticket to closed
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function close(int $id)
    {
        $ticket = Ticket::findOrFail($id);
        $ticket->status = 'closed';

        $ticket->save();

        return redirect(session('overview-url'));
    }

    /**
     * stores a comment to specific ticket in storage
     *
     * @param int $ticketId
     * @param Request $request
     * @return Application|Response|ResponseFactory|RedirectResponse
     */
    public function postComment(int $ticketId, Request $request): Application|ResponseFactory|Response|RedirectResponse
    {
        $ticket = Ticket::findOrFail($ticketId);

        if (Auth::user()->isAdmin == 1 || Auth::id() === $ticket->creatorUserId || Auth::id() === $ticket->referredUserId) {
            Comment::create(
                [
                    'comment' => $request->input('newComment'),
                    'ticketId' => $ticketId,
                    'userId' => Auth::id(),
                ]
            );

            return redirect(session('overview-url'));
        }
        return response('You are not allowed to post a comment on this ticket', 403);
    }
}

<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Category::create(['name' => 'General']);
        Category::create(['name' => 'Question']);
        Category::create(['name' => 'Internal request']);
        Category::create(['name' => 'Offer']);
        Category::create(['name' => 'Phone call']);
        Category::create(['name' => 'Bug']);

        User::create([
         'name' => 'Admin User',
         'email' => 'admin@test.de',
         'password' => Hash::make('admin'),
         'isAdmin' => true,
        ]);

        User::create([
         'name' => 'Normal User',
         'email' => 'test@test.de',
         'password' => Hash::make('test'),
         'isAdmin' => false,
        ]);
    }
}
